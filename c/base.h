#ifndef __BASE_H__
#define __BASE_H__

#include "cJSON.h"

#define pi 3.1415926

#define STRAIGHT_TYPE 0
#define SWITCH_TYPE 1
#define BEND_TYPE 2

extern int totalLaps;

typedef struct sPiece
{
    union {
        double length;
        double radius;
    };

    double angle;

    unsigned char type;
    unsigned char isSwitch;
} piece;

typedef struct sLane
{
    double distFromCenter;
    int    index;
} lane;

typedef struct sTrackInfo
{
    piece* pieceAry;
    lane*  laneAry;

    int    pieceNum;
    int    laneNum;
} trackInfo;

typedef struct sVarArray
{
	void** elemAry;
    int size;
    int idx;
} varArray;

typedef struct sCarTpInfo
{
	varArray* tpInfoAry;
	char*     name;
} carTpInfo;

typedef struct sTpInfo
{
    double inPieceDist;
	double vel;
    double accel;
	double angle;
    double throttle;
    int lap;
	int pieceIdx;
	int stLane;
	int endLane;
} tpInfo;

extern void setupMyCar(char*);

extern varArray* varArrayInit(int size);
extern void* varArrayGetAtIdx(varArray* varAry, int idx);
extern void varArrayAdd(varArray* varAry, void* data);

extern void tpInfoInit(int carNum);
extern void tpInfoAddCar(int idx, char* name);
extern void tpInfoShowCars();
extern void tpInfoAddTp(char* carName, double angle, int pieceIdx, double inPieceDist, int stLane, int endLane, int lap);
extern void tpInfoAddTpThrottle(char* carName, double throttle);

extern void showTps();

#endif
