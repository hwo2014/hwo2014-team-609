#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "base.h"

extern cJSON *throttle_msg(double throttle);

trackInfo* track = NULL;
char* myCar = NULL;
int totalLaps = 0;

void setupMyCar(char* name)
{
    myCar = (char*)calloc(strlen(name)+1, sizeof(char));
    strcpy(myCar, name);

    printf("My Car : %s\n", myCar);
}

double getPieceRadius(int idx, int stLaneIdx, int endLaneIdx) 
{
    if (idx >= track->pieceNum) {
		printf("Error : desired to get piece length of piece %d\n", idx);
		return 0.0;
	}
    if (BEND_TYPE == track->pieceAry[idx].type) {
		//printf("Warning : desired to get length of bend piece\n");
        double radius = track->pieceAry[idx].radius;

		if (track->pieceAry[idx].angle > 0) {
			radius -= (track->laneAry[stLaneIdx].distFromCenter + 
                       track->laneAry[endLaneIdx].distFromCenter)/2;
        }
		else {
			radius += (track->laneAry[stLaneIdx].distFromCenter + 
                       track->laneAry[endLaneIdx].distFromCenter)/2;
		}

		return radius;
	}
	else {
		return 0;
	}
}

/* race (track) related APIs */ 
double getPieceLength(int idx, int stLaneIdx, int endLaneIdx) 
{
    if (idx >= track->pieceNum) {
		printf("Error : desired to get piece length of piece %d\n", idx);
		return 0.0;
	}
    if (BEND_TYPE == track->pieceAry[idx].type) {
		//printf("Warning : desired to get length of bend piece\n");
        double radius = track->pieceAry[idx].radius;

		if (track->pieceAry[idx].angle > 0) {
			radius -= (track->laneAry[stLaneIdx].distFromCenter + 
                       track->laneAry[endLaneIdx].distFromCenter)/2;
        }
		else {
			radius += (track->laneAry[stLaneIdx].distFromCenter + 
                       track->laneAry[endLaneIdx].distFromCenter)/2;
		}

		return radius * 2 * pi * fabs(track->pieceAry[idx].angle / 360);
	}
	else {
		return track->pieceAry[idx].length;
	}
}


/************************************************************************
 * Description:
 *     variable length array.
 *     Each entry in the array is a void*, user need to cast the void* 
 *     to user defined data structure.
 *
 * APIs:
 *     varArrayInit : initialize the array
 *     varArrayGetAtIdx : get the element at array index
 *     varArrayAdd : add a data to the array
 ************************************************************************/
varArray* varArrayInit(int size) 
{
	varArray* varAry = (varArray*)calloc(1, sizeof(varArray));
    varAry->size = size;
    varAry->elemAry = (void**)calloc(size, sizeof(void*));
    varAry->idx = -1;
    return varAry;
}

void* varArrayGetAtIdx(varArray* varAry, int idx)
{
	if (NULL == varAry || -1 == varAry->idx || varAry->idx < idx || idx >= varAry->size || idx < 0) {
		return NULL;
	}

    return varAry->elemAry[idx];
}

void varArrayAdd(varArray* varAry, void* data)
{
	if (NULL == varAry) return;

	if (varAry->idx >= varAry->size-1) {
		varAry->elemAry = (void**)realloc(varAry->elemAry, sizeof(void*)*varAry->size*2);
		varAry->size *= 2;
	}
    
    varAry->elemAry[++varAry->idx] = data;
}

/************************************************************************
 * Description:
 *     time point data structure
 *
 *
 *
 ************************************************************************/
static carTpInfo* S_carTpInfoAry = NULL;
static int S_carNum = 0;
static double S_timeUnit = 1.0/60;

static int getCarIdx(char* name)
{
	int idx;
	for (idx = 0; idx < S_carNum; ++idx) {
		if (!strcmp(name, S_carTpInfoAry[idx].name)) {
			return idx;
		}
	}
	return -1;
}

static varArray* getTpInfoAryOfCar(char* name)
{
	int idx;
	for (idx = 0; idx < S_carNum; ++idx) {
		if (!strcmp(name, S_carTpInfoAry[idx].name)) {
			return S_carTpInfoAry[idx].tpInfoAry;
		}
	}
	return NULL;
}

void tpInfoSetTimeUnit(double unit)
{
	S_timeUnit = unit;
}

void tpInfoInit(int carNum)
{
	int idx;
	S_carNum = carNum;

	S_carTpInfoAry = (carTpInfo*)calloc(carNum, sizeof(carTpInfo));
	
	for (idx = 0; idx < carNum; ++idx) {
		S_carTpInfoAry[idx].tpInfoAry = varArrayInit(500);  
	}
}

void tpInfoAddCar(int idx, char* name)
{
	if (NULL == S_carTpInfoAry) return;

	S_carTpInfoAry[idx].name = (char*)calloc(strlen(name)+1, sizeof(char));
	strcpy(S_carTpInfoAry[idx].name, name);
}

void tpInfoShowCars()
{
	int idx;
	for (idx = 0; idx < S_carNum; ++idx) {
		printf("Car %d : %s\n", idx, S_carTpInfoAry[idx].name);
	}
}

int tpInfoGetTpNum(char* name)
{	
	int carIdx = getCarIdx(name);

	if (-1 == carIdx) return 0;

	return S_carTpInfoAry[carIdx].tpInfoAry->idx+1;	
}

/* if the idx is negative,
 * -1 means latest time point
 * -2 means previous time point
 * -3 means second previous time point
 * ...
 */
tpInfo* tpInfoGetTp(char* name, int idx)
{
	varArray* tpInfoAry = getTpInfoAryOfCar(name);

	if (NULL == tpInfoAry) {
		printf("Can't find time point information of car %s\n", name);
		return NULL;
	}

	/* if the input index is -1, means the last time point is desired */
	if (idx < 0) {
		idx = tpInfoAry->idx + idx + 1;
	}

    tpInfo* info = (tpInfo*)varArrayGetAtIdx(tpInfoAry, idx);
		
	//printf("tpInfoGetTp of idx %d [%p]\n", idx, info);
	return info;
}

void showTpInfo(int idx, tpInfo* info, FILE* fp)
{
    fprintf(fp, "[%d] :\n", idx);
	fprintf(fp, "\tpieceIdx    : %d\n", info->pieceIdx);
    fprintf(fp, "\tinPieceDist : %g\n", info->inPieceDist);
	fprintf(fp, "\tradius      : %g\n", getPieceRadius(info->pieceIdx, info->stLane, info->endLane));
	fprintf(fp, "\tvel         : %g\n", info->vel);
    fprintf(fp, "\taccel       : %g\n", info->accel);
	fprintf(fp, "\tangle       : %g\n", info->angle);
    fprintf(fp, "\tthrottle    : %g\n", info->throttle);
    fprintf(fp, "\tlap         : %d\n", info->lap);
	fprintf(fp, "\tstLane      : %d\n", info->stLane);
	fprintf(fp, "\tendLane     : %d\n", info->endLane);
	
	if (stdout != fp) {
		fp = stdout;
	    fprintf(fp, "[%d] :\n", idx);
		fprintf(fp, "\tpieceIdx    : %d\n", info->pieceIdx);
	    fprintf(fp, "\tinPieceDist : %g\n", info->inPieceDist);
		fprintf(fp, "\tradius      : %g\n", getPieceRadius(info->pieceIdx, info->stLane, info->endLane));
		fprintf(fp, "\tvel         : %g\n", info->vel);
	    fprintf(fp, "\taccel       : %g\n", info->accel);
		fprintf(fp, "\tangle       : %g\n", info->angle);
	    fprintf(fp, "\tthrottle    : %g\n", info->throttle);
	    fprintf(fp, "\tlap         : %d\n", info->lap);
		fprintf(fp, "\tstLane      : %d\n", info->stLane);
		fprintf(fp, "\tendLane     : %d\n", info->endLane);
	}
}

void showTps() 
{
    int idx;
    int tpNum = tpInfoGetTpNum(myCar);
    tpInfo* info = NULL;

	printf("showTps\n");
	
	FILE* fp = fopen("log", "w");

    for (idx = 0; idx < tpNum; ++idx) {
        info = tpInfoGetTp(myCar, idx);
        showTpInfo(idx, info, fp);
    }

	fclose(fp);
}

double calcDist(
	int initPieceIdx, 
	double initInPieceDist, 
    int initLane,
	int endPieceIdx, 
	double endInPieceDist,
    int endLane)
{
	double dist = 0.0;
	int idx;
	int stIdx = 0, edIdx = 0;
	
	printf("init piece %d (offset %g) - end piece %d (offset %g)\n", 
            initPieceIdx, initInPieceDist, endPieceIdx, endInPieceDist);

	if (initPieceIdx == endPieceIdx) {
		//printf("pieceDist %g - %g = %g\n", endInPieceDist, initInPieceDist, endInPieceDist - initInPieceDist);
		return endInPieceDist - initInPieceDist;
	}
	
	dist += getPieceLength(initPieceIdx, initLane, endLane) - initInPieceDist;
	dist += endInPieceDist;
	
	stIdx = initPieceIdx + 1; 
	
	if (endPieceIdx < initPieceIdx) {
		edIdx = track->pieceNum;		
	}
	else {
		edIdx = endPieceIdx;
	}

	for (idx = stIdx; idx < edIdx; ++idx) {
		dist += getPieceLength(idx, initLane, endLane);
	}
	return dist; 
}

void tpInfoAddTp(
	char* carName, 
	double angle, 
	int pieceIdx, 
	double inPieceDist, 
	int stLane, 
	int endLane,
	int lap)
{	
 	varArray* tpInfoAry = getTpInfoAryOfCar(carName);
	tpInfo* newTp = (tpInfo*)calloc(1, sizeof(tpInfo));	
	tpInfo* oldTp = tpInfoGetTp(carName, -1);
	double dist = 0.0;
	double oldVel = 0.0;
	
	//printf("tpInfoAddTp to car %s [%p]\n", carName, tpInfoAry);
	newTp->lap = lap;
	newTp->pieceIdx = pieceIdx;
    newTp->inPieceDist = inPieceDist;
	newTp->angle = angle;
	newTp->stLane = stLane;
	newTp->endLane = endLane;
    
	if (NULL == oldTp) {
		dist = calcDist(0, 0, stLane, pieceIdx, inPieceDist, endLane);
		oldVel = 0.0;
	}
	else {
		dist = calcDist(oldTp->pieceIdx, oldTp->inPieceDist, stLane, pieceIdx, inPieceDist, endLane);
		oldVel = oldTp->vel;
	}

	printf("Dist : %g\n", dist);

	newTp->vel = dist/S_timeUnit;
    newTp->accel = (newTp->vel - oldVel) / S_timeUnit;

	varArrayAdd(tpInfoAry, (void*)newTp);

	printf("Add tp with vel %g accel %g with angle %g\n", newTp->vel, newTp->accel, newTp->angle);
}

void tpInfoAddTpThrottle(char* carName, double throttle)
{	
	tpInfo* oldTp = tpInfoGetTp(carName, -1);

    if (NULL != oldTp) {
        oldTp->throttle = throttle;
    }
}

double tpInfoGetVel(char* name, int idx)
{
    tpInfo* info = tpInfoGetTp(name, idx);

    if (NULL == info) return 0.0;

	return info->vel;
}

double tpInfoGetAccel(char* name, int idx)
{
    tpInfo* info = tpInfoGetTp(name, idx);

    if (NULL == info) return 0.0;

	return info->accel;
}

cJSON* bStartAlgo(char* carName)
{

    cJSON* msg = throttle_msg(throttle);
    tpInfoAddTpThrottle(carName, throttle);

    return msg;
}


