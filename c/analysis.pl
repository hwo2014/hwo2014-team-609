#!/usr/bin/perl

open (FILE, $ARGV[0]) or die ("Can't open file $ARGV[0]");

@lines = <FILE>;
close(FILE);

my $idx = -1;
my $value;
my $prevVel = 0;
my $prevAccel = 0;
my $prevThrottle = 0;

sub getVal {
	$string = @_[0];
	#print "$string\n";
	@vars = split(":", $string);
	return $vars[1];
}

print "[index] prevVel prevAccel prevThrottle Radius curVel curAccel curAngle\n";

foreach $line (@lines) {
	chomp($line);

	if ($line =~ "inPieceDist") {
		++$idx;
		print "\n[$idx] $prevVel $prevAccel $prevThrottle ";               
    }
	elsif ($line =~ "radius") {
		$value = &getVal($line);
		print "$value ";
	}
	elsif ($line =~ "vel") {
		$value = &getVal($line);
		$prevVel = $value;
        print "$value ";
    }
	elsif ($line =~ "accel") {
		$value = &getVal($line);
		$prevAccel = $value;
		print "$value ";
    }
	elsif ($line =~ "throttle") {
		$value = &getVal($line);
		$prevThrottle = $value;
		#print "$value ";
    }
	elsif ($line =~ "angle") {
		$value = &getVal($line);
		print "$value";
	}
}

print "\n";

