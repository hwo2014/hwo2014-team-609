#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "cJSON.h"
#include "base.h"
#include "algo.h"

extern char* myCar;
extern cJSON *throttle_msg(double throttle);

cJSON* learningStage(char* carName)
{
    double throttle = (double)random()/(double)RAND_MAX;
    printf("learningStage : %g\n", throttle);

    cJSON* msg = throttle_msg(throttle);
    tpInfoAddTpThrottle(carName, throttle);

    return msg;
}

cJSON* bStarAlgo(char* carName)
{
    double throttle = 0.6;
	printf("bStarAlgo\n");
    cJSON* msg = throttle_msg(throttle);
    tpInfoAddTpThrottle(carName, throttle);

    return msg;
}

cJSON* trentAlgo(char* myCar)
{
	printf("trentAlgo\n");
	return NULL;
}

cJSON* tsungtseAlgo(char* myCar)
{
	printf("tsungtseAlgo\n");
    int tpNum = tpInfoGetTpNum(myCar);
    int idx = tpNum-1;
    tpInfo* info = NULL;
    info = tpInfoGetTp(myCar, idx);
    double throttle = 0.6;
    lane* curLane;

    int curIdx = info->pieceIdx;
    int lookahead = 2;

    if (info->vel >= 540) {
        lookahead = 3;
    }
    int pieceNext = curIdx + lookahead;
    if (pieceNext > track->pieceNum) {
        pieceNext = pieceNext % track->pieceNum;
    }
    piece* curPiece = &track->pieceAry[curIdx];
    piece* next2Piece = &track->pieceAry[pieceNext];
    if (next2Piece->type == BEND_TYPE) {
        if (next2Piece->radius>= 200) {
            throttle = 0.80;
        } else {
            throttle = 0.60;
            if (info->angle > 40 || info->angle < -40) {
                printf("angle adjust\n");
                throttle = 0.42;
            }
            if (next2Piece->angle < 0) {
                curLane = &track->laneAry[info->stLane];
                if (curLane->distFromCenter <=0) {
                    throttle = 0.42;
                    printf("inner lane\n");
                    //make_msg("switchLane", cJSON_CreateString("left"));
                }
            } else {
                curLane = &track->laneAry[info->stLane];
                if (curLane->distFromCenter >=0) {
                    throttle = 0.42;
                    printf("inner lane\n");
                    //make_msg("switchLane", cJSON_CreateString("left"));
                }
            }
            /*
            if (info->vel >=540) {
                printf("rush break\n");
                throttle = 0.42;
            }*/

        }

    } else {
        throttle = 1.0;

    }
    if (curPiece->type == BEND_TYPE) {
        if (info->angle > 40 || info->angle < -40) {
            printf("cur angle adjust\n");
            throttle = 0.30;
        }

        if (info->vel >=540) {
            printf("rush break\n");
            throttle = 0.42;
        }

    }

    cJSON* msg = throttle_msg(throttle);
    tpInfoAddTpThrottle(carName, throttle);

    return msg;

}

cJSON* raceAlgo(char* carName, int lap, char algoType)
{
	switch (algoType) {
		case YJ_ALGO:
		    if (lap < totalLaps-1) {
        		return learningStage(myCar);
		    }	
		    return bStarAlgo(myCar);
		case TRENT_ALGO:
			return trentAlgo(myCar);
		case TSUNG_TSE_ALGO:
			return tsungtseAlgo(myCar);
		default:
			return bStarAlgo(myCar);
	}
	return NULL;
}
