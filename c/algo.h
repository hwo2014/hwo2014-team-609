#ifndef __ALGO_H__
#define __ALGO_H__

extern cJSON* raceAlgo(char* carName, int lap, char algoType);
extern cJSON* learningStage(char* carName);
extern cJSON* bStarAlgo(char* carName);

#define YJ_ALGO 1
#define TRENT_ALGO 2
#define TSUNG_TSE_ALGO 3

#endif


