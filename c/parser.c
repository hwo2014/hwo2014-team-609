#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "cJSON.h"
#include "base.h"
#include "algo.h"

extern trackInfo* track;
extern char* myCar;
extern int totalLaps;

static void showPiece(piece* curPiece)
{
    switch (curPiece->type) {
        case STRAIGHT_TYPE:
            printf("\tType   : straight line\n");
            printf("\tLength : %g\n", curPiece->length);
            break;
        case BEND_TYPE:
            printf("\tType   : bend line\n");
            printf("\tRadius : %g\n", curPiece->radius);
            printf("\tAngle  : %g\n", curPiece->angle);
            break;
    }
    
    if (curPiece->isSwitch) {
        printf("\tisSwitch\n");
    }
}

static void showLane(lane* curLane)
{
    printf("distance from center : %g\n", curLane->distFromCenter);
    printf("index                : %d\n", curLane->index);
}

static void showTrack() 
{
    if (NULL == track) return;

    int idx;

    for (idx = 0; idx < track->pieceNum; ++idx) {
        printf("[%d]\n", idx);
        showPiece(&track->pieceAry[idx]);
    }
    printf("\n");
    for (idx = 0; idx < track->laneNum; ++idx) {
        printf("[%d]\n", idx);
        showLane(&track->laneAry[idx]);
    }
}

static void 
parseRaceInfo(cJSON* msg) 
{
    cJSON* data_obj = cJSON_GetObjectItem(msg, "data");
    cJSON* race_obj = cJSON_GetObjectItem(data_obj, "race");
    cJSON* track_obj = cJSON_GetObjectItem(race_obj, "track");
    cJSON* pieceAry_obj = cJSON_GetObjectItem(track_obj, "pieces");
    cJSON* laneAry_obj = cJSON_GetObjectItem(track_obj, "lanes");
	cJSON* car_obj = cJSON_GetObjectItem(race_obj, "cars");
    cJSON* racesession_obj = cJSON_GetObjectItem(race_obj, "raceSession");
    
    int pieceNum = cJSON_GetArraySize(pieceAry_obj);
    int laneNum = cJSON_GetArraySize(laneAry_obj);

    int idx = 0;

    printf("piece num %d\n", pieceNum);
        
    track = (trackInfo*)calloc(1, sizeof(trackInfo));
    track->pieceNum = pieceNum;
    track->pieceAry = (piece*)calloc(pieceNum, sizeof(piece));
    track->laneNum = laneNum;
    track->laneAry = (lane*)calloc(laneNum, sizeof(lane));

    cJSON* piece_obj = NULL, *lane_obj = NULL;
    cJSON* obj = NULL;
    piece* curPiece = NULL;
    lane* curLane = NULL;

    for (idx = 0; idx < pieceNum; ++idx) {
        curPiece = &(track->pieceAry[idx]);

        piece_obj = cJSON_GetArrayItem(pieceAry_obj, idx);
    
        if (NULL != (obj = cJSON_GetObjectItem(piece_obj, "length"))) {
            curPiece->length = obj->valuedouble;

            curPiece->type = STRAIGHT_TYPE;

            if (NULL != (obj = cJSON_GetObjectItem(piece_obj, "switch"))) {
                curPiece->isSwitch = 1;
            }
        }
        else {
            obj = cJSON_GetObjectItem(piece_obj, "radius");
            curPiece->radius = obj->valuedouble;
            obj = cJSON_GetObjectItem(piece_obj, "angle");
            curPiece->angle = obj->valuedouble;
            curPiece->type = BEND_TYPE;
            
            if (NULL != (obj = cJSON_GetObjectItem(piece_obj, "switch"))) {
                curPiece->isSwitch = 1;
            }
        }
    }

    printf("lane num : %d\n", laneNum);

    for (idx = 0; idx < laneNum; ++idx) {   
        lane_obj = cJSON_GetArrayItem(laneAry_obj, idx);
        curLane = &(track->laneAry[idx]);
        obj = cJSON_GetObjectItem(lane_obj, "distanceFromCenter");
        curLane->distFromCenter = obj->valuedouble;
        obj = cJSON_GetObjectItem(lane_obj, "index");
        curLane->index = obj->valueint;
    }
    
    showTrack();

    /* parse car info */
    int carNum = cJSON_GetArraySize(car_obj);
    cJSON* id_obj = NULL;
	cJSON* name_obj = NULL;
	
	printf("carNum %d\n", carNum);

	tpInfoInit(carNum);

	for (idx = 0; idx < carNum; ++idx) {
		obj = cJSON_GetArrayItem(car_obj, idx);
		id_obj = cJSON_GetObjectItem(obj, "id");
		name_obj = cJSON_GetObjectItem(id_obj, "name");
						
		tpInfoAddCar(idx, name_obj->valuestring);
	}

	tpInfoShowCars();

    obj = cJSON_GetObjectItem(racesession_obj, "laps");
    if( obj == NULL ) {
        obj = cJSON_GetObjectItem(racesession_obj, "durationMs");
    }
    totalLaps = obj->valueint;
}

cJSON* parseCarPosition(cJSON* msg)
{
	cJSON* data_obj = cJSON_GetObjectItem(msg, "data");
	cJSON* car_obj = NULL;
	cJSON* obj = NULL;
	cJSON* obj1 = NULL;
	cJSON* obj2 = NULL;

	int carNum = cJSON_GetArraySize(data_obj);
	int idx;
	double angle = 0.0;
	int pieceIdx = 0;
	double inPieceDist = 0.0;
	int stLane = 0;
	int endLane = 0;
	char* carName = NULL;
	int lap;
	
	//printf("parseCarPosition\n");

	for (idx = 0; idx < carNum; ++idx) {
		//printf("Get Car %d\n", idx);
		car_obj = cJSON_GetArrayItem(data_obj, idx);
		/* get car name */
		obj = cJSON_GetObjectItem(car_obj, "id");
		obj1 = cJSON_GetObjectItem(obj, "name");
		carName = obj1->valuestring;
		
		/* get angle */
		obj = cJSON_GetObjectItem(car_obj, "angle");
		angle = obj->valuedouble;

		/* get piece info */
		obj = cJSON_GetObjectItem(car_obj, "piecePosition");
		obj1 = cJSON_GetObjectItem(obj, "pieceIndex");
		pieceIdx = obj1->valueint;
		
		obj1 = cJSON_GetObjectItem(obj, "inPieceDistance");
		inPieceDist = obj1->valuedouble;

		obj1 = cJSON_GetObjectItem(obj, "lane");
		obj2 = cJSON_GetObjectItem(obj1, "startLaneIndex");
		stLane = obj2->valueint;

		obj2 = cJSON_GetObjectItem(obj1, "endLaneIndex");
		endLane = obj2->valueint;
		
		obj1 = cJSON_GetObjectItem(obj, "lap");
		lap = obj1->valueint;			
		
		tpInfoAddTp(carName, angle, pieceIdx, inPieceDist, stLane, endLane, lap);
	}

	return raceAlgo(myCar, lap, YJ_ALGO);
}

cJSON* parseMsg(cJSON* msg)
{
    cJSON* msg_type = cJSON_GetObjectItem(msg, "msgType");

    char* msg_type_name = msg_type->valuestring;

    //printf("msg_type %s\n", msg_type_name);

    if (!strcmp(msg_type_name, "join")) {
        
    }
    else if (!strcmp(msg_type_name, "yourCar")) {

    }
    else if (!strcmp(msg_type_name, "gameInit")) {
        parseRaceInfo(msg);
    }
    else if (!strcmp(msg_type_name, "gameStart")) {

    }
    else if (!strcmp(msg_type_name, "carPositions")) {
		return parseCarPosition(msg);
    }
    else if (!strcmp(msg_type_name, "gameEnd")) {
        printf("gameEnd\n");
        showTps();
    }
    else if (!strcmp(msg_type_name, "tournamentEnd")) {

    }
    else if (!strcmp(msg_type_name, "crash")) {
        //notifyCrashAngle();
        printf("Crash\n");
    }
    else if (!strcmp(msg_type_name, "spawn")) {

    }
    else if (!strcmp(msg_type_name, "lapFinished")) {
        printf("lapFinished\n");

    }
    else if (!strcmp(msg_type_name, "dnf")) {

    }
    else if (!strcmp(msg_type_name, "finish")) {

    }
    return NULL;
}
