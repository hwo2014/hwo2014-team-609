function analysis(filename);

fid = fopen(filename, 'r');

idx = 0;

while (1) 
	[index prevVel prevA prevThrottle radius curVel curA curAngle count] = fscanf(fid, '%s %g %g %g %g %g %g %g', "C");
    if (0 == count)
		break;
	endif

    prevVelAry(idx) = prevVel;
    prevAccelAry(idx) = prevA;
    prevThrottleAry(idx) = prevThrottle;
    radiusAry(idx) = radius;
    curVelAry(idx) = curVel;
    curAccelAry(idx) = curA;
    curAngleAry(idx) = curAngle;
    
    ++idx;
endwhile 

fclose(fid);

endfunction
